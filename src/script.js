var UPPER_LIMIT = 1;
var DIVIDER = 500000;
var SEX_PROBABILITY = 0.9;
var MUTATION_PROBABILITY = 0.1;

// функция для генерации рандомного числа от 0 до 5
function random() {
    var seedForSign = Math.floor(Math.random() * 10) + 0;
    var sign = seedForSign%2==0 ? -1 : 1;
    return (sign*(Math.floor(Math.random() * UPPER_LIMIT*DIVIDER) + 0)/DIVIDER);
}

function foo(x) {
    return -3.6*Math.exp(-3.5*Math.pow(x,2))*Math.sign(Math.cos(31*x-7));
}

// класс "Особь"
function Individual() {
    this.value;
    this.binary;
    this.sign;
    this.fitness;
    
    this.setFitness = function() {
        this.fitness = foo(this.value);
    }
    
    this.getFitness = function() {
        return this.fitness;
    }
    
    this.initByBinary = function(_binary) {
        var firstSymbol = parseInt(_binary.charAt(0), 10);
        this.sign = firstSymbol == 1 ? 1 : 0; // если первый символ в бинарном числе 1, значит оно представляет собой отрицательное число, если 0, значит положительное
        this.binary = _binary;
        
        // set value
        var tmpValue = parseInt(_binary.substr(1), 2); // берем все биты, кроме 1го, так как он отражает знак
        tmpValue = this.sign == 1 ? -1*tmpValue : tmpValue;
        this.value = tmpValue/DIVIDER;
    }
    
    this.initByValue = function(_value) {
        this.value = _value;
        
        // set binary
        var tmpValue = parseInt(_value*DIVIDER.toFixed(), 10);
        this.sign = tmpValue < 0 ? 1 : 0; // если отрицательное число, то в sign 1, если положительное, то в sign 0
        var tmpBinary = tmpValue.toString(2);
        
        // хак для того, чтобы убрать знак "-" при преобразовании отрицательного числа в двоичное
        if(this.sign == 1) {
            tmpBinary = tmpBinary.substr(1);
        }

        // добавим столько 0, чтобы получилось 19 бит (не 20, потому что 1ый, он для знака)
        for(var i = 19-tmpBinary.length; i > 0; i--) {
            tmpBinary = "0"+tmpBinary;
        }
        
        // добавим 20ый бит, знаковый
        if(this.sign == 1) {
            tmpBinary = "1"+tmpBinary;
        } else {
            tmpBinary = "0"+tmpBinary;
        }
        
        this.binary = tmpBinary;
        //console.log("Binary length: "+tmpBinary.length);
    }
    
    this.getValue = function() {
        return this.value;
    }
    
    this.getBinary = function() {
        return this.binary;
    }    
}
var individual = null;
var population = [];

(function() {
	console.log('Programm started');
    
	
    //individual.initByBinary("100000000000000000101");
    
    
    for(var i = 0; i < 50; i++) {
        individual = new Individual();
        individual.initByValue(random());
        population.push(individual);
    }
    
    console.log(population);
})();